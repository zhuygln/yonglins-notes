Introduction to Nucleosynthesis
===========================================
Yonglin is working on the impact of neutrinos on nucleosyntheis, especially on r-process in Neutron Ster Merger(for a good review, check `Neutron Star Mergers and Nucleosynthesis of Heavy Elements <http://www.annualreviews.org/doi/abs/10.1146/annurev-nucl-101916-123246>`_)

.. admonition:: Why are we interested in r-process?
   :class: hint

   Because it is very important.

.. admonition:: WHY
   :class: warning

   AAAAAAAAAA


.. index:: XXXXXXXXX

XXXXXX
---------------------------------


.. admonition:: Projection Technique
   :class: note

  First of all define a diagonalizing operator :math:`\hat D` which just keeps the diagonal elements and simply drops the off diagonal elements. We see that :math:`1-\hat D` will eliminate all diagonal elements.

   We can define the diagonalized density matrix as :math:`\hat \rho_d = \hat D \hat \rho` and off-diagonalized density matrix as :math:`\hat \rho_{od} = (1-\hat D)\hat \rho`. As an application,

   .. math::
      \hat \rho = \hat \rho_d + \hat \rho_{od} .


.. admonition:: What to Do?
   :class: warning

   wait

Refs & Notes
-------------------

