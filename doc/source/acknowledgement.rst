Acknowledgement
====================

I would like to thank my advisor Professor Gail C. McLaughlin for the support on my research. I would also like to thank Yue Yang, Brett Deaton, and A. C. Malkus for the many useful discussions and tutorials.
