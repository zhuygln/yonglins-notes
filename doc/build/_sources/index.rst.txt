.. Yonglin's Notes documentation master file, created by
   sphinx-quickstart on Tue Sep  5 16:35:16 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Yonglin's Neutrino Notes!
===========================================

.. [1] `Cowan–Reines neutrino experiment <https://en.wikipedia.org/wiki/Cowan%E2%80%93Reines_neutrino_experiment>`_


.. figure:: assets/FirstNeutrinoEventAnnotated.jpg
   :align: center

support me:

.. image:: assets/tips-neutrino-4c924c.svg
   :target: https://gratipay.com/~yzhu14/






.. toctree::
   :maxdepth: 3
   :caption: Contents:
   :numbered:


   preliminary/index.rst
   mass/index.rst
   experiments/index.rst
   oscillations/index.rst
   instability/index.rst
   pictures/index.rst
   models/index.rst
   statistical-physics/index.rst
   geometric-phase/index.rst
   astrophysics/index.rst
   gravity/index.rst
   cosmology/index.rst
   misc/index.rst
   ref.rst
   acknowledgement.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`search`
